/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import Business.Role.EventsOfficeRole;
import Business.Role.Role;
import Business.Role.VolunteeringOfficeRole;
import java.util.HashSet;

/**
 *
 * @author Lakhani
 */
public class EventManagementEnterprise extends Enterprise {
    public EventManagementEnterprise(String name) {
        super(name, Enterprise.EnterpriseType.EventManagementEnterprise);
    }

    @Override
    public HashSet<Role> getSupportedRole() {
        roles.add(new EventsOfficeRole());
        roles.add(new VolunteeringOfficeRole());
        return roles;
    }
}
