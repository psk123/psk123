package Business;

import Business.Employee.Employee;
import Business.Role.SystemAdminRole;
import Business.UserAccount.UserAccount;
import java.io.BufferedReader;
import java.io.FileReader;
import Business.GovernmentFunding.Tax;
import Business.GovernmentFunding.TaxDirectory;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *
 * @author Pooja Alone <your.name at your.org>
 */
public class ConfigureASystem {
     private TaxDirectory taxDirectory;
    public static EcoSystem configure(){
        
        EcoSystem system = EcoSystem.getInstance();
        
        Employee employee = system.getEmployeeDirectory().createEmployee("xxx");
        
        UserAccount ua = system.getUserAccountDirectory().createUserAccount("admin", "admin", employee, new SystemAdminRole());
        
        return system;
    }
    
}
