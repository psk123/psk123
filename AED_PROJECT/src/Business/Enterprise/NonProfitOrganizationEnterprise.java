/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import Business.Role.HomelessRole;
import Business.Role.OldAgeHomeRole;
import Business.Role.Role;
import Business.Role.WomenCareRole;
import java.util.HashSet;

/**
 *
 * @author Lakhani
 */
public class NonProfitOrganizationEnterprise extends Enterprise {
    public NonProfitOrganizationEnterprise(String name) {
        super(name, Enterprise.EnterpriseType.NonProfitOrganizationEnterprise);
    }

    @Override
    public HashSet<Role> getSupportedRole() {
        roles.add(new HomelessRole());
        roles.add(new OldAgeHomeRole());
        roles.add(new WomenCareRole());
        return roles;
    }

}
