/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Employement.JobDirectory;
import Business.Role.Role;
import java.util.HashSet;

/**
 *
 * @author Saurabh
 */
public class JobPostingManagementOrganization extends Organization{
    
    private JobDirectory jobDirectory;
    
    public JobPostingManagementOrganization(){
        super(Organization.Type.JobPostingManagementOrganization.getValue());
        jobDirectory = new JobDirectory();
    }

    public JobDirectory getJobDirectory() {
        return jobDirectory;
    }

    public void setJobDirectory(JobDirectory jobDirectory) {
        this.jobDirectory = jobDirectory;
    }

    @Override
    public HashSet<Role> getSupportedRole() {
        return null;
    }

}
