/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Employement;

/**
 *
 * @author Pooja Alone <your.name at your.org>
 */
public class Job {
    
    private String jobTitle;
    private int minAge;
    private int maxAge;
    private String jobFor;
    private int hourlyWage;
    private int positionsLeft;
    private String CompanyName;

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public int getMinAge() {
        return minAge;
    }

    public void setMinAge(int minAge) {
        this.minAge = minAge;
    }

    public int getMaxAge() {
        return maxAge;
    }

    public void setMaxAge(int maxAge) {
        this.maxAge = maxAge;
    }

    public String getJobFor() {
        return jobFor;
    }

    public void setJobFor(String jobFor) {
        this.jobFor = jobFor;
    }

    public int getHourlyWage() {
        return hourlyWage;
    }

    public void setHourlyWage(int hourlyWage) {
        this.hourlyWage = hourlyWage;
    }

    public int getPositionsLeft() {
        return positionsLeft;
    }

    public void setPositionsLeft(int positionsLeft) {
        this.positionsLeft = positionsLeft;
    }

    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String CompanyName) {
        this.CompanyName = CompanyName;
    }

    @Override
    public String toString() {
        return jobTitle;
    }
    
}
