/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Employee.EmployeeDirectory;
import Business.Role.Role;
import Business.UserAccount.UserAccountDirectory;
import Business.WorkQueue.WorkQueue;
import java.util.ArrayList;
import java.util.HashSet;


public abstract class Organization {

    private String name;
    private EmployeeDirectory employeeDirectory;
    private UserAccountDirectory userAccountDirectory;
    private int organizationID;
    private static int counter;
    public HashSet<Role> roles;
    private WorkQueue workQueue;
    
    
    public enum Type{
        HomelessOrganization("Homeless Organization"),
        GroupDonarOrganization("Group Donar Organization"),
        IndividualDonarOrganization("Individual Donar Organization"),
        OldAgeHomeOrganization("Old Age Home Organization"),
        WomenCareOrganization("Women Care Organization"),
        FundingOrganization("Funding Organization"),
        TaxOrganization("Tax Department Organization"),
        UniversityOrganization("University Organization"),
        JobPostingManagementOrganization("Job Posting Management Organization"),
        SuperMarketOrganization("Super Market Organization"),
        EventManagementOrganization("Event Management Organization"),
        VolunteerManagementOrganization("Volunteer Management Organization"),
        JudiciaryOrganization("Judiciary Organization");
        
        private String value;
        private Type(String value) {
            this.value = value;
        }
        public String getValue() {
            return value;
        }
    }

    public Organization(String name) {
        this.name = name;
        employeeDirectory = new EmployeeDirectory();
        userAccountDirectory = new UserAccountDirectory();
        organizationID = counter;
        roles = new HashSet<>();
        workQueue = new WorkQueue();
        ++counter;
    }

    public abstract HashSet<Role> getSupportedRole();
    
    public UserAccountDirectory getUserAccountDirectory() {
        return userAccountDirectory;
    }

    public int getOrganizationID() {
        return organizationID;
    }

    public EmployeeDirectory getEmployeeDirectory() {
        return employeeDirectory;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public WorkQueue getWorkQueue() {
        return workQueue;
    }

    public void setWorkQueue(WorkQueue workQueue) {
        this.workQueue = workQueue;
    }
    
    @Override
    public String toString() {
        return name;
    }
}
