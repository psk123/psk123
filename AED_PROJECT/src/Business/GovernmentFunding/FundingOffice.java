/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.GovernmentFunding;

import Business.WorkQueue.WorkQueue;

/**
 *
 * @author Lakhani
 */
public class FundingOffice {
    private String name;
    private WorkQueue workQueue;
    private double fundsLeft;
    private double initialFunds;

    public FundingOffice() {
        workQueue = new WorkQueue();
    }

    public WorkQueue getWorkQueue() {
        return workQueue;
    }

    public void setWorkQueue(WorkQueue workQueue) {
        this.workQueue = workQueue;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getFundsLeft() {
        return fundsLeft;
    }

    public void setFundsLeft(double fundsLeft) {
        this.fundsLeft = fundsLeft;
    }

    public double getInitialFunds() {
        return initialFunds;
    }

    public void setInitialFunds(double initialFunds) {
        this.initialFunds = initialFunds;
    }
    
    @Override
    public String toString() {
        return name;
    }
}
