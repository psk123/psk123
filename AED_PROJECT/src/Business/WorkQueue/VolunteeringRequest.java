/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import Business.Event.Event;
import Business.Organization.Organization;

/**
 *
 * @author Pooja Alone <your.name at your.org>
 */
public class VolunteeringRequest extends WorkRequest{
    
    private String feedback;
    private Event event;
    private Organization recievingOrganization;

    public VolunteeringRequest() {
        event = new Event();
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public Organization getRecievingOrganization() {
        return recievingOrganization;
    }

    public void setRecievingOrganization(Organization recievingOrganization) {
        this.recievingOrganization = recievingOrganization;
    }
    
}
