/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.Role;
import java.util.HashSet;

/**
 *
 * @author Saurabh
 */
public class SuperMarketOrganization extends Organization{
    
    public SuperMarketOrganization(){
        super(Organization.Type.SuperMarketOrganization.getValue());
    }

    @Override
    public HashSet<Role> getSupportedRole() {
        return null;
    }

}
