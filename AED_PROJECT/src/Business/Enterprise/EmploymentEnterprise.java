/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import Business.Role.JobPostingManagementOfficeRole;
import Business.Role.Role;
import java.util.HashSet;

/**
 *
 * @author Lakhani
 */
public class EmploymentEnterprise extends Enterprise {
    public EmploymentEnterprise(String name) {
        super(name, Enterprise.EnterpriseType.EmploymentEnterprise);
    }

   @Override
    public HashSet<Role> getSupportedRole() {
        roles.add(new JobPostingManagementOfficeRole());
        return roles;
    }
}
