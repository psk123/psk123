/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Event;

import Business.UserAccount.UserAccount;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Pooja Alone <your.name at your.org>
 */
public class Event {
    
    private String eventID;
    private String eventName;
    private String eventVenue;
    private Date eventDate;
    private int volunteerNeeded;
    private static int count;
    private ArrayList<UserAccount> volunteerAccount;

    public Event() {
        eventID = "EV" + (++count);
        volunteerAccount = new ArrayList<>();
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventVenue() {
        return eventVenue;
    }

    public void setEventVenue(String eventVenue) {
        this.eventVenue = eventVenue;
    }

    public Date getEventDate() {
        return eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    public int getVolunteerNeeded() {
        return volunteerNeeded;
    }

    public void setVolunteerNeeded(int volunteerNeeded) {
        this.volunteerNeeded = volunteerNeeded;
    }

    public ArrayList<UserAccount> getVolunteerAccount() {
        return volunteerAccount;
    }

    public void setVolunteerAccount(ArrayList<UserAccount> volunteerAccount) {
        this.volunteerAccount = volunteerAccount;
    }

    @Override
    public String toString() {
        return eventName;
    }
    
}
