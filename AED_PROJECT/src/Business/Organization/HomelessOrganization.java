/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;


import Business.NonProfit.Product;
import Business.NonProfit.ProductCatalogue;
import Business.Role.Role;
import java.util.ArrayList;
import java.util.HashSet;


public class HomelessOrganization extends Organization{

    private ProductCatalogue productCatalogue;
    
    public HomelessOrganization() {
        super(Organization.Type.HomelessOrganization.getValue());
        productCatalogue = new ProductCatalogue();
    }

    @Override
    public HashSet<Role> getSupportedRole() {
        return null;
    }

    public ProductCatalogue getProductCatalogue() {
        return productCatalogue;
    }

    public void setProductCatalogue(ProductCatalogue productCatalogue) {
        this.productCatalogue = productCatalogue;
    }

}