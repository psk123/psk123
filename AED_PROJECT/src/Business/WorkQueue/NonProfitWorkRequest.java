/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import Business.NonProfit.Product;
import Business.Organization.Organization;

/**
 *
 * @author Lakhani
 */
public class NonProfitWorkRequest extends WorkRequest{
    private String feedback;
    private Product product;
    private Organization recievingOrganization;

    public NonProfitWorkRequest() {
        product = new Product();
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
    
    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public Organization getRecievingOrganization() {
        return recievingOrganization;
    }

    public void setRecievingOrganization(Organization recievingOrganization) {
        this.recievingOrganization = recievingOrganization;
    }
    
}
