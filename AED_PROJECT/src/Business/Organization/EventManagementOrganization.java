/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Event.EventCatalogue;
import Business.Role.Role;
import java.util.HashSet;

/**
 *
 * @author Saurabh
 */
public class EventManagementOrganization extends Organization{
    private EventCatalogue eventCatalogue;
    
    public EventManagementOrganization(){
        super(Organization.Type.EventManagementOrganization.getValue());
        eventCatalogue = new EventCatalogue();
    }

    @Override
    public HashSet<Role> getSupportedRole() {
        return null;
    }

    public EventCatalogue getEventCatalogue() {
        return eventCatalogue;
    }

    public void setEventCatalogue(EventCatalogue eventCatalogue) {
        this.eventCatalogue = eventCatalogue;
    }
    
}
