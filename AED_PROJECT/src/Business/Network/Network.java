/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Network;

import Business.Enterprise.EnterpriseDirectory;
import Business.GovernmentFunding.TaxDirectory;

/**
 *
 * @author Pooja Alone <your.name at your.org>
 */
public class Network{
    
    private String name;
    private EnterpriseDirectory enterpriseDirectory;
    private TaxDirectory taxDirectory;

    public Network() {
        enterpriseDirectory = new EnterpriseDirectory();
        taxDirectory = new TaxDirectory();
    }
    
    public EnterpriseDirectory getEnterpriseDirectory() {
        return enterpriseDirectory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    public TaxDirectory getTaxDirectory() {
        return taxDirectory;
    }

    public void setTaxDirectory(TaxDirectory taxDirectory) {
        this.taxDirectory = taxDirectory;
    }
}
