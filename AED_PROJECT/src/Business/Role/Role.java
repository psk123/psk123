/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import javax.swing.JPanel;

/**
 *
 * @author Pooja Alone <your.name at your.org>
 */
public abstract class Role {
    
    public enum RoleType{
        
        
        DonorAdminRole("DonorAdminRole"),
        SystemAdmin("SystemAdmin"),
        EmploymentAdminRole("EmploymentAdminRole"),
        EventManagementAdminRole("EventManagementAdminRole"),
        EventsOfficeRole("EventsOfficeRole"),
        FundingDepartmentRole("FundingDepartmentRole"),
        GovernmentFundingAdminRole("GovernmentFundingAdminRole"),
        HomelessRole("HomelessRole"),
        JudiciaryAdminRole("JudiciaryAdminRole"),
        JudiciaryOfficeRole("JudiciaryOfficeRole"),
        JobPostingManagementOfficeRole("JobPostingManagementOfficeRole"),
        OldAgeAdminRole("OldAgeAdminRole"),
        SuperMarketAdminRole("SuperMarketAdminRole"),
        TaxAdminRole("TaxAdminRole"),
        WomenCareAdminRole("WomenCareAdminRole"),
        IndividualDonorRole("IndividualDonorRole"),
        GroupDonorRole("GroupDonorRole");
        
        private String value;
        private RoleType(String value){
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return value;
        }
    }
    
    public abstract JPanel createWorkArea(JPanel userProcessContainer, 
            UserAccount account, 
            Organization organization, 
            Enterprise enterprise,
            Network inNetwork,
            EcoSystem business);

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }
}