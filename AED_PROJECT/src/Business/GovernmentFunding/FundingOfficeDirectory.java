/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.GovernmentFunding;

import java.util.ArrayList;

/**
 *
 * @author Lakhani
 */
public class FundingOfficeDirectory {
    private ArrayList<FundingOffice> fundingList;

    public FundingOfficeDirectory() {
        fundingList = new ArrayList<>();
    }

    public ArrayList<FundingOffice> getFundingList() {
        return fundingList;
    }

    public void setFundingList(ArrayList<FundingOffice> fundingList) {
        this.fundingList = fundingList;
    }
    
    public FundingOffice createFundingOffice(){
        FundingOffice fundingOffice = new FundingOffice();
        fundingList.add(fundingOffice);
        return fundingOffice;
    }
    
    public void removeFundingOffcie(FundingOffice fundingOffice){
        fundingList.remove(fundingOffice);
    }
}
