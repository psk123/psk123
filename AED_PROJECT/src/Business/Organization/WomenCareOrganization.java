/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.NonProfit.Product;
import Business.NonProfit.ProductCatalogue;
import Business.Role.Role;
import java.util.ArrayList;
import java.util.HashSet;

/**
 *
 * @author Saurabh
 */
public class WomenCareOrganization extends Organization{
    
    private ProductCatalogue productCatalogue;
    
    public WomenCareOrganization(){
        super(Organization.Type.WomenCareOrganization.getValue());
        productCatalogue = new ProductCatalogue();
    }

    @Override
    public HashSet<Role> getSupportedRole() {
       return null;
    }

    public ProductCatalogue getProductCatalogue() {
        return productCatalogue;
    }

    public void setProductCatalogue(ProductCatalogue productCatalogue) {
        this.productCatalogue = productCatalogue;
    }

}
