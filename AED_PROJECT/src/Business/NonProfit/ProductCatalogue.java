/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.NonProfit;

import java.util.ArrayList;

/**
 *
 * @author Lakhani
 */
public class ProductCatalogue {
    private ArrayList<Product> productList;

    public ProductCatalogue() {
        productList = new ArrayList<>();
    }

    public ArrayList<Product> getProductList() {
        return productList;
    }

    public void setProductList(ArrayList<Product> productList) {
        this.productList = productList;
    }
    
    public Product createProduct() {
        Product product = new Product();
        productList.add(product);
        return product;
    }
}
