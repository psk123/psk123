package Business;

import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.Organization.Organization;
import Business.Role.DonorAdminRole;
import Business.Role.EmploymentAdminRole;
import Business.Role.EventManagementAdminRole;
//import Business.Role.EnterpriseAdminRole;
import Business.Role.GovernmentFundingAdminRole;
//import Business.Role.JudiciaryAdminRole;
import Business.Role.NonProfitAdminRole;
import Business.Role.Role;
import Business.Role.SystemAdminRole;
import Business.UserAccount.UserAccount;
import java.util.ArrayList;
import java.util.HashSet;

/**
 *
 * @author Pooja Alone <your.name at your.org>
 */
public class EcoSystem extends Organization {

    private static EcoSystem business;
    private ArrayList<Network> networkList;

    public static EcoSystem getInstance() {
        if (business == null) {
            business = new EcoSystem();
        }
        return business;
    }

    private EcoSystem() {
        super(null);
        networkList = new ArrayList<>();
    }

    public ArrayList<Network> getNetworkList() {
        return networkList;
    }
    
    public static void setInstance(EcoSystem system) {
        business=system;
    }

    public Network createAndAddNetwork() {
        Network network = new Network();
        networkList.add(network);
        return network;
    }

    @Override
    public HashSet<Role> getSupportedRole() {
        roles.add(new SystemAdminRole());
        roles.add(new DonorAdminRole());
        roles.add(new NonProfitAdminRole());
        roles.add(new GovernmentFundingAdminRole());
        roles.add(new EmploymentAdminRole());
        roles.add(new EventManagementAdminRole());
//        roles.add(new JudiciaryAdminRole());
        return roles;
    }

    public static boolean checkIfUsernameIsUnique(String username) {

        for (Network network : business.getNetworkList()) {

            for (Enterprise enterprise : network.getEnterpriseDirectory().getEnterpriseList()) {
                for (UserAccount ua : enterprise.getUserAccountDirectory().getUserAccountList()) {
                    if (ua.getUsername().equals(username)) {
                        return false;
                    }
                }

                for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()) {
                    for (UserAccount ua : organization.getUserAccountDirectory().getUserAccountList()) {
                        if (ua.getUsername().equals(username)) {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }
}