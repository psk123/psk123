/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.GovernmentFunding;

import java.util.ArrayList;

/**
 *
 * @author Pooja Alone <your.name at your.org>
 */
public class TaxDirectory {
     private ArrayList<Tax> taxList;

    public TaxDirectory() {
         taxList = new ArrayList<Tax>();

    }

    public ArrayList<Tax> getTaxList() {
        return taxList;
    }

    public void setTaxList(ArrayList<Tax> taxList) {
        this.taxList = taxList;
    }
     public Tax addTax(){
    Tax tax = new Tax();
    taxList.add(tax);
    return tax;
}


}
