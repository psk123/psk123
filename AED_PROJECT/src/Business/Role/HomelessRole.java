/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import UserInterface.NonProfitEnterprise.Homeless.HomelessWorkAreaJPanel;
import javax.swing.JPanel;


public class HomelessRole extends Role{

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, Network inNetwork, EcoSystem business) {
        return new HomelessWorkAreaJPanel(userProcessContainer, organization, account, inNetwork, business);
    }
    
}
