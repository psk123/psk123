/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.GovernmentFunding;

/**
 *
 * @author Pooja Alone <your.name at your.org>
 */
public class Tax {
    
    private String month;
    private int year;
    private double totalTax;
    private double usefulTax;

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double getTotalTax() {
        return totalTax;
    }

    public void setTotalTax(double totalTax) {
        this.totalTax = totalTax;
    }

    public double getUsefulTax() {
        return usefulTax;
    }

    public void setUsefulTax(double usefulTax) {
        this.usefulTax = usefulTax;
    }
}
