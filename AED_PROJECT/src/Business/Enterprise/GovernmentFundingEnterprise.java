/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import Business.Role.FundingDepartmentRole;
import Business.Role.Role;
import Business.Role.TaxDepartmentRole;
import java.util.HashSet;

/**
 *
 * @author Lakhani
 */
public class GovernmentFundingEnterprise extends Enterprise {
     public GovernmentFundingEnterprise(String name) {
        super(name, Enterprise.EnterpriseType.GovernmentFundingEnterprise);
    }
     
    @Override
    public HashSet<Role> getSupportedRole() {
        roles.add(new FundingDepartmentRole());
        roles.add(new TaxDepartmentRole());
        return roles;
    }
}
