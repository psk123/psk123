/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Employement;

import java.util.ArrayList;

/**
 *
 * @author Pooja Alone <your.name at your.org>
 */
public class JobDirectory {
    
    private ArrayList<Job> jobList;

    public JobDirectory() {
        jobList = new ArrayList<>();
    }

    public ArrayList<Job> getJobList() {
        return jobList;
    }

    public void setJobList(ArrayList<Job> jobList) {
        this.jobList = jobList;
    }
    
    public Job createJob()
    {
        Job job = new Job();
        jobList.add(job);
        return job;
    }
    
}
