/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Event;

import java.util.ArrayList;

/**
 *
 * @author Pooja Alone <your.name at your.org>
 */
public class EventCatalogue {
    
    private ArrayList<Event> eventList;

    public EventCatalogue() {
        eventList =  new ArrayList<>();
    }

    public ArrayList<Event> getEventList() {
        return eventList;
    }

    public void setEventList(ArrayList<Event> eventList) {
        this.eventList = eventList;
    }
    
    public Event createEvent()
    {
        Event event = new Event();
        eventList.add(event);
        return event;
    }
}
