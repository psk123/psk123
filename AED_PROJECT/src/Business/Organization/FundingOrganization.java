/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.GovernmentFunding.FundingOfficeDirectory;
import Business.Role.Role;
import java.util.HashSet;

/**
 *
 * @author Saurabh
 */
public class FundingOrganization extends Organization{
    private FundingOfficeDirectory fundingOfficeDirectory;
    private double fundLeft;
    private double initialFunds;
    public FundingOrganization(){
        super(Organization.Type.FundingOrganization.getValue());
        fundingOfficeDirectory = new FundingOfficeDirectory();
               
    }

    public FundingOfficeDirectory getFundingOfficeDirectory() {
        return fundingOfficeDirectory;
    }

    public void setFundingOfficeDirectory(FundingOfficeDirectory fundingOfficeDirectory) {
        this.fundingOfficeDirectory = fundingOfficeDirectory;
    }

    public double getFundLeft() {
        return fundLeft;
    }

    public void setFundLeft(double fundLeft) {
        this.fundLeft = fundLeft;
    }

    public double getInitialFunds() {
        return initialFunds;
    }

    public void setInitialFunds(double initialFunds) {
        this.initialFunds = initialFunds;
    }

    @Override
    public HashSet<Role> getSupportedRole() {
        return null;
    }

    
}
