/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import Business.Role.GroupDonorRole;
import Business.Role.IndividualDonorRole;
import Business.Role.Role;
import Business.Role.SuperMarketRole;
import java.util.ArrayList;
import java.util.HashSet;

/**
 *
 * @author Lakhani
 */
public class DonorEnterprise extends Enterprise {
    public DonorEnterprise(String name) {
        super(name, EnterpriseType.DonorEnterprise);
    }
    
    @Override
    public HashSet<Role> getSupportedRole() {
        roles.add(new IndividualDonorRole());
        roles.add(new GroupDonorRole());
        roles.add(new SuperMarketRole());
        return roles;
    }

}
