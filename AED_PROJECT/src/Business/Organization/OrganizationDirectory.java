/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Organization.Organization.Type;
import java.util.ArrayList;


public class OrganizationDirectory {
    
    private ArrayList<Organization> organizationList;

    public OrganizationDirectory() {
        organizationList = new ArrayList<>();
    }

    public ArrayList<Organization> getOrganizationList() {
        return organizationList;
    }
    
    public Organization createOrganization(Type type){
        Organization organization = null;
        if (type.getValue().equals(Type.HomelessOrganization.getValue())) {
            organization = new HomelessOrganization();
            organizationList.add(organization);
                } else if (type.getValue().equals(Type.OldAgeHomeOrganization.getValue())) {
            organization = new OldAgeHomeOrganization();
            organizationList.add(organization);
        } else if (type.getValue().equals(Type.WomenCareOrganization.getValue())) {
            organization = new WomenCareOrganization();
            organizationList.add(organization);
        } else if (type.getValue().equals(Type.JobPostingManagementOrganization.getValue())) {
            organization = new JobPostingManagementOrganization();
            organizationList.add(organization);
        } else if (type.getValue().equals(Type.SuperMarketOrganization.getValue())) {
            organization = new SuperMarketOrganization();
            organizationList.add(organization);
        } else if (type.getValue().equals(Type.EventManagementOrganization.getValue())) {
            organization = new EventManagementOrganization();
            organizationList.add(organization);
        } else if (type.getValue().equals(Type.FundingOrganization.getValue())) {
            organization = new FundingOrganization();
            organizationList.add(organization);
        } else if (type.getValue().equals(Type.TaxOrganization.getValue())) {
            organization = new TaxOrganization();
            organizationList.add(organization);
        } else if (type.getValue().equals(Type.GroupDonarOrganization.getValue())) {
            organization = new GroupDonarOrganization();
            organizationList.add(organization);
        } else if (type.getValue().equals(Type.IndividualDonarOrganization.getValue())) {
            organization = new IndividualDonarOrganization();
            organizationList.add(organization);
        } else if (type.getValue().equals(Type.VolunteerManagementOrganization.getValue())) {
            organization = new VolunteerManagementOrganization();
            organizationList.add(organization);
        }
        return organization;
    }
}