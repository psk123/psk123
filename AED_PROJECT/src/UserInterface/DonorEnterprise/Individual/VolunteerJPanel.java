/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.DonorEnterprise.Individual;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Enterprise.EventManagementEnterprise;
import Business.Event.Event;
import Business.Network.Network;
import Business.Organization.EventManagementOrganization;
import Business.Organization.Organization;
import Business.Organization.VolunteerManagementOrganization;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.EventRequest;
import Business.WorkQueue.VolunteeringRequest;
import Business.WorkQueue.WorkQueue;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Lakhani
 */
public class VolunteerJPanel extends javax.swing.JPanel {

    private JPanel container;
    private EcoSystem system;
    private UserAccount userAccount;
    private Organization organization;
    private Network network;
    private VolunteerManagementOrganization volunteerManagementOrganization;

    /**
     * Creates new form VolunteerJPanel
     */
    public VolunteerJPanel(JPanel container, EcoSystem system, UserAccount userAccount, Organization organization, Network network) {
        initComponents();
        this.container = container;
        this.system = system;
        this.userAccount = userAccount;
        this.organization = organization;
        this.network = network;
        volunteerManagementOrganization = (VolunteerManagementOrganization) getVolunteeringOrg();
        populateEventsList();
        populateMyRequestsList();
    }

    private void populateEventsList() {

        DefaultTableModel model = (DefaultTableModel) Eventstbl.getModel();

        model.setRowCount(0);
        if (organization.getWorkQueue() == null) {
            WorkQueue w = new WorkQueue();
            organization.setWorkQueue(w);
        }
        EventManagementOrganization eventManagementOrganization = (EventManagementOrganization) getEventOfficeOrg();
        for (WorkRequest workRequest : eventManagementOrganization.getWorkQueue().getWorkRequestList()) {
            if (workRequest instanceof EventRequest) {
                Object[] row = new Object[4];
                EventRequest eventRequest = (EventRequest) workRequest;
                row[0] = eventRequest.getEvent();
                row[2] = eventRequest.getEvent().getEventDate();
                row[1] = eventRequest.getEvent().getEventVenue();
                row[3] = eventRequest.getEvent().getVolunteerNeeded();
                model.addRow(row);
            }

        }
    }

    private void populateMyRequestsList() {
        DefaultTableModel model = (DefaultTableModel) myrequesttbl.getModel();

        model.setRowCount(0);
        if (organization.getWorkQueue() == null) {
            WorkQueue w = new WorkQueue();
            organization.setWorkQueue(w);
        }
        
        for (WorkRequest work : volunteerManagementOrganization.getWorkQueue().getWorkRequestList()) {
            if (work instanceof VolunteeringRequest) {
                Object[] row = new Object[4];
                VolunteeringRequest volunteeringRequest = (VolunteeringRequest) work;
                row[0] = volunteeringRequest.getEvent();
                row[1] = volunteeringRequest.getEvent().getVolunteerNeeded();
                row[2] = work;
                row[3] = volunteeringRequest.getFeedback();
                model.addRow(row);
            }
        }
    }

    public Organization getEventOfficeOrg() {
        Enterprise entr = null;
        for (Enterprise enterprise : network.getEnterpriseDirectory().getEnterpriseList()) {
            if (enterprise instanceof EventManagementEnterprise) {
                entr = enterprise;
                break;
            }
        }
        Organization reciOrganization = null;
        for (Organization recOrganization : entr.getOrganizationDirectory().getOrganizationList()) {
            if (recOrganization instanceof EventManagementOrganization) {
                reciOrganization = recOrganization;
                break;
            }
        }
        return reciOrganization;
    }
    
     public Organization getVolunteeringOrg() {
        Enterprise entr = null;
        for (Enterprise enterprise : network.getEnterpriseDirectory().getEnterpriseList()) {
            if (enterprise instanceof EventManagementEnterprise) {
                entr = enterprise;
                break;
            }
        }
        Organization reciOrganization = null;
        for (Organization recOrganization : entr.getOrganizationDirectory().getOrganizationList()) {
            if (recOrganization instanceof VolunteerManagementOrganization) {
                reciOrganization = recOrganization;
                break;
            }   
        }
        return reciOrganization;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel6 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel7 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        Eventstbl = new javax.swing.JTable();
        volunteerbtn = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        myrequesttbl = new javax.swing.JTable();
        bckbtn = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 153, 0));
        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 3));

        jLabel6.setFont(new java.awt.Font("Castellar", 1, 24)); // NOI18N
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("Volunteering work area");
        jLabel6.setOpaque(true);

        jSeparator1.setBackground(new java.awt.Color(0, 0, 0));
        jSeparator1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 3));

        jLabel7.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel7.setText("Events List");

        Eventstbl.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Event Name", "Venue", "Date", "No. of Volunteers"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(Eventstbl);

        volunteerbtn.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        volunteerbtn.setText("Volunteer");
        volunteerbtn.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        volunteerbtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                volunteerbtnActionPerformed(evt);
            }
        });

        btnDelete.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        btnDelete.setText("Delete request");
        btnDelete.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel8.setText("My Request List");

        myrequesttbl.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Event Name", "No. of Volunteers", "Status", "Feedback"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane3.setViewportView(myrequesttbl);
        if (myrequesttbl.getColumnModel().getColumnCount() > 0) {
            myrequesttbl.getColumnModel().getColumn(0).setResizable(false);
            myrequesttbl.getColumnModel().getColumn(1).setResizable(false);
            myrequesttbl.getColumnModel().getColumn(2).setResizable(false);
            myrequesttbl.getColumnModel().getColumn(3).setResizable(false);
        }

        bckbtn.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        bckbtn.setText("Back<<");
        bckbtn.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        bckbtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bckbtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, 891, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(328, 328, 328)
                        .addComponent(jLabel8)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(340, 340, 340)
                        .addComponent(jLabel7))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(60, 60, 60)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(bckbtn, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(396, 396, 396)
                                .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 610, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 610, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(volunteerbtn, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 11, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel7)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(volunteerbtn, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addComponent(jLabel8)
                .addGap(37, 37, 37)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bckbtn, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 40, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void volunteerbtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_volunteerbtnActionPerformed
        int selectedRow = Eventstbl.getSelectedRow();
        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(null, "Please select the row.", "Warning", JOptionPane.WARNING_MESSAGE);
        } else {
            VolunteeringRequest volunteeringRequest = new VolunteeringRequest();
            Event requestedEvent = (Event) Eventstbl.getValueAt(selectedRow, 0);
            volunteeringRequest.setEvent(requestedEvent);
            volunteeringRequest.setSender(userAccount);
            volunteeringRequest.setRecievingOrganization(volunteerManagementOrganization);
            organization.getWorkQueue().getWorkRequestList().add(volunteeringRequest);
            userAccount.getWorkQueue().getWorkRequestList().add(volunteeringRequest);
            volunteerManagementOrganization.getWorkQueue().getWorkRequestList().add(volunteeringRequest);
            JOptionPane.showMessageDialog(null, "Request has been sent", "Information", JOptionPane.INFORMATION_MESSAGE);
            populateMyRequestsList();
        }
    }//GEN-LAST:event_volunteerbtnActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        // TODO add your handling code here:
        int selectedRow = Eventstbl.getSelectedRow();
        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(null, "Please select the row to delete the request", "Warning", JOptionPane.WARNING_MESSAGE);
        } else {

            VolunteeringRequest volunteeringRequest = (VolunteeringRequest) myrequesttbl.getValueAt(selectedRow, 2);
            organization.getWorkQueue().getWorkRequestList().remove(volunteeringRequest);
            userAccount.getWorkQueue().getWorkRequestList().remove(volunteeringRequest);
            Organization recOrg = volunteeringRequest.getRecievingOrganization();
            recOrg.getWorkQueue().getWorkRequestList().remove(volunteeringRequest);
            JOptionPane.showMessageDialog(null, "You have successfully deleted the request");
            populateEventsList();
        }
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void bckbtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bckbtnActionPerformed
        // TODO add your handling code here:
        container.remove(this);
        CardLayout layout = (CardLayout) container.getLayout();
        layout.previous(container);
    }//GEN-LAST:event_bckbtnActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable Eventstbl;
    private javax.swing.JButton bckbtn;
    private javax.swing.JButton btnDelete;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTable myrequesttbl;
    private javax.swing.JButton volunteerbtn;
    // End of variables declaration//GEN-END:variables
}
