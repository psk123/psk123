/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import UserInterface.NonProfitEnterprise.OldAge.OldAgeWorkAreaJPanel;
import javax.swing.JPanel;

/**
 *
 * @author Saurabh
 */
public class OldAgeHomeRole extends Role{

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, Network inNetwork, EcoSystem business) {
        return new OldAgeWorkAreaJPanel(userProcessContainer, organization, account, inNetwork, business);
    }
    
    
}
