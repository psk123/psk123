/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.Role;
import java.util.HashSet;

/**
 *
 * @author Saurabh
 */
public class VolunteerManagementOrganization extends Organization{
    
    public VolunteerManagementOrganization(){
        super(Organization.Type.VolunteerManagementOrganization.getValue());
    }

    @Override
    public HashSet<Role> getSupportedRole() {
        return null;
    }
    
}
