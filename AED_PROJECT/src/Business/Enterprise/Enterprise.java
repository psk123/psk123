/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;
import Business.Organization.OrganizationDirectory;
import Business.Organization.Organization;
import Business.Role.Role;
import java.util.HashSet;
/**
 *
 * @author Lakhani
 */
public class Enterprise extends Organization{
    private EnterpriseType enterpriseType;
    private OrganizationDirectory organizationDirectory;

    public EnterpriseType getEnterpriseType() {
        return enterpriseType;
    }

    public OrganizationDirectory getOrganizationDirectory() {
        return organizationDirectory;
    }

    @Override
    public HashSet<Role> getSupportedRole() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public enum EnterpriseType {
        NonProfitOrganizationEnterprise("NonProfitOrganization Enterprise"), GovernmentFundingEnterprise("Government Funding Enterprise"),
        EmploymentEnterprise("Employment Enterprise"), EventManagementEnterprise("EventManagement Enterprise"),
        JudiciaryEnterprise("Judiciary Enterprise"), DonorEnterprise("Donor Enterprise");
        private String value;

        private EnterpriseType(String value) {
            this.value= value;
        }

        public String getValue() {
            return value;
        }
        
    }
     public Enterprise(String name, EnterpriseType enterpriseType) {
        super(name);
        this.enterpriseType = enterpriseType;
        organizationDirectory = new OrganizationDirectory();
    }
}
