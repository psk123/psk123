/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import Business.GovernmentFunding.FundingOffice;
import Business.Organization.Organization;

/**
 *
 * @author Lakhani
 */
public class FundsRequest extends WorkRequest{
    private String feedback;
    private Organization receivingOrganization;
    private int fundsNeeded;
    private FundingOffice fundingOffice;

    public FundingOffice getFundingOffice() {
        return fundingOffice;
    }

    public void setFundingOffice(FundingOffice fundingOffice) {
        this.fundingOffice = fundingOffice;
    }

   
    public FundsRequest() {
        
    }

    public int getFundsNeeded() {
        return fundsNeeded;
    }

    public void setFundsNeeded(int fundsNeeded) {
        this.fundsNeeded = fundsNeeded;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public Organization getReceivingOrganization() {
        return receivingOrganization;
    }

    public void setReceivingOrganization(Organization receivingOrganization) {
        this.receivingOrganization = receivingOrganization;
    }
    
}
