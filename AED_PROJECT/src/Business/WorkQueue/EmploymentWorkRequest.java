/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import Business.Employement.Job;
import Business.Organization.Organization;

/**
 *
 * @author Pooja Alone <your.name at your.org>
 */
public class EmploymentWorkRequest extends WorkRequest{
    
    private String feedback;
    private Job job;
    private Organization recievingOrganization;

    public EmploymentWorkRequest() {
        job = new Job();
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public Organization getRecievingOrganization() {
        return recievingOrganization;
    }

    public void setRecievingOrganization(Organization recievingOrganization) {
        this.recievingOrganization = recievingOrganization;
    }
    
}
